package com.elishanarida.sccregistration

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val name = "Name"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Log.d("Main Activt", "Main")

        main_register.setOnClickListener {
            registerSucess()
        }
    }

    fun registerSucess() {
        val firstName = main_firstName.text.toString()
        val lastName = main_lastName.text.toString()

        Toast.makeText(this, "Welcome $firstName $lastName", Toast.LENGTH_LONG).show()
    }
}
